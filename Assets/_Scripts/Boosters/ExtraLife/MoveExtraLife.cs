﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _Scripts.Unit;
using Random=UnityEngine.Random;


namespace _Scripts.Boosters
{
    
    public class MoveExtraLife : MoveUnit
    {
        private static int _orderInLayer = 0;

        private void Awake()
        {
            this._unitOffset = Constants.ItemOffset;
            this._spriteSize = Constants.GetSpriteSize(gameObject);
            gameObject.GetComponent<SpriteRenderer>().sortingOrder = _orderInLayer++;
            this.RestartUnit();
            gameObject.SetActive(false);
        }

        public override void RestartUnit()
        {
            this.transform.position = this.RandomPosition();
            this._speed = RandomSpeed();
            this.gameObject.GetComponent<DestroyExtraLife>().Vertical = this._vertical;
            this.gameObject.GetComponent<DestroyExtraLife>().Positive = this._positive;
        }

    }

}


﻿using _Scripts.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace _Scripts.Boosters
{
    public class DestroyExtraLife : DestroyUnit
    {
        
        protected override void OnTriggerEnter2D(Collider2D other)
        {
            if ( other.gameObject.CompareTag("Player") )
            {
                this.Deactivate();
                other.gameObject.GetComponent<Player.HealthPlayer>().Heal();
            }
        }
        
        protected override void Deactivate()
        {
            gameObject.SetActive(false);
            gameObject.GetComponent<MoveExtraLife>().RestartUnit();
        }

        protected override bool ToBeDeactivated(bool vertical, bool positive){
            
            if( vertical && positive )
            {
                if (this.gameObject.transform.position.y > Constants.WorldUpperBound() + (-Constants.ItemOffset) )
                {
                    return true;
                }
            }

            if ( vertical && !positive )
            {
                if (this.gameObject.transform.position.y < Constants.WorldLowerBound() - (-Constants.ItemOffset) )
                {
                    return true;
                }
            }

            if (!vertical && positive)
            {
                if (this.gameObject.transform.position.x > Constants.WorldRightBound() + (-Constants.ItemOffset) )
                {
                    return true;
                }
            }

            if (!vertical && !positive)
            {
                if (this.gameObject.transform.position.x < Constants.WorldLeftBound() - (-Constants.ItemOffset) )
                {
                    return true;
                }
            }
        
            return false;
        }
    }

}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace _Scripts.Food
{
    public class DestroyFood : DestroyUnit
    {
        
        protected override void OnTriggerEnter2D(Collider2D other)
        {
            if ( other.gameObject.CompareTag("Player") )
            {
                this.Deactivate();
                other.gameObject.GetComponent<Player.ScorePlayer>().Score += FoodScore();
            }

            if (other.gameObject.CompareTag("Enemy"))
            {
                this.Deactivate();
            }
            
        }
        
        protected override void Deactivate()
        {
            gameObject.SetActive(false);
            gameObject.GetComponent<MoveFood>().RestartUnit();
        }

        protected override bool ToBeDeactivated(bool vertical, bool positive){
            
            if( vertical && positive )
            {
                if (this.gameObject.transform.position.y > Constants.WorldUpperBound() + (-Constants.ItemOffset) )
                {
                    return true;
                }
            }

            if ( vertical && !positive )
            {
                if (this.gameObject.transform.position.y < Constants.WorldLowerBound() - (-Constants.ItemOffset) )
                {
                    return true;
                }
            }

            if (!vertical && positive)
            {
                if (this.gameObject.transform.position.x > Constants.WorldRightBound() + (-Constants.ItemOffset) )
                {
                    return true;
                }
            }

            if (!vertical && !positive)
            {
                if (this.gameObject.transform.position.x < Constants.WorldLeftBound() - (-Constants.ItemOffset) )
                {
                    return true;
                }
            }
        
            return false;
        }
        
        private int FoodScore()
        {
            switch ( gameObject.tag )
            {
                case "Candy":
                    return Constants.ScoreCandy;
                case "Chocolate":
                    return Constants.ScoreChocolate;
                case "Pizza":
                    return Constants.ScorePizza;
            }

            return 0;
        }
    }

}


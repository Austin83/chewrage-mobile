﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _Scripts.Unit;
using Random=UnityEngine.Random;


namespace _Scripts.Food
{
    
    public class MoveFood : MoveUnit
    {
        private static int _orderInLayer = 0;

        private void Awake()
        {
            this._unitOffset = Constants.ItemOffset;
            this._spriteSize = Constants.GetSpriteSize(gameObject);
            gameObject.GetComponent<SpriteRenderer>().sortingOrder = _orderInLayer++;
            this.RestartUnit();
        }

        public override void RestartUnit()
        {
            this.transform.position = this.RandomPosition();
            this._speed = RandomSpeed();
            this.gameObject.GetComponent<DestroyFood>().Vertical = this._vertical;
            this.gameObject.GetComponent<DestroyFood>().Positive = this._positive;
            this.gameObject.SetActive(true);
        }

    }

}


﻿using UnityEngine;

namespace _Scripts.Unit
{
	public abstract class MoveUnit : MonoBehaviour {
	
		[SerializeField] protected float _speedAverage;
        
		protected float _speed;
		protected bool _vertical;
		protected bool _positive;
		protected Vector3 _spriteSize;
		protected float _unitOffset;
		
		public float SpeedAverage
		{
			get { return _speedAverage; }
			set { _speedAverage = value; }
		}

		public float Speed
		{
			get { return _speed; }
			set { _speed = value; }
		}
        
		private float TranslateVertical
		{
			get
			{
				if ( !_vertical )
				{
					return 0.0f;
				}
            
				if ( _positive )
				{
					return _speed;
				}
				else
				{
					return -_speed;

				}
			}         
		}
        
		private float TranslateHorizontal
		{
			get
			{
				if ( _vertical )
				{
					return 0.0f;
				}
            
				if ( _positive )
				{
					return _speed;
				}
				else
				{
					return -_speed;

				}
			}           
		}


		private void Move()
		{
			transform.Translate(new Vector3( this.TranslateHorizontal  * Time.deltaTime, this.TranslateVertical  * Time.deltaTime, 0.0f));
		}
		
		protected float RandomSpeed(){
			return  Random.Range( this._speedAverage * Constants.SpeedMultiplierMinimum , 
				this._speedAverage * Constants.SpeedMultiplierMaximum);
		}
		
		private void Update()
		{
			Move();
		}

		public abstract void RestartUnit();
		
		protected Vector3 RandomPosition()
		{
			float x = 0.0f;
			float y = 0.0f;

			switch (Random.Range(1, 5))
			{
				case 1: //up
					y = Constants.WorldUpperBound() + this._spriteSize.y + this._unitOffset;
					this._vertical = true;
					this._positive = false;
					break;
				case 2: //down
					y = Constants.WorldLowerBound() - this._spriteSize.y - this._unitOffset;
					this._vertical = true;
					this._positive = true;
					break;
				case 3: //right
					x = Constants.WorldRightBound() + this._spriteSize.x + this._unitOffset;
					this._vertical = false;
					this._positive = false;
					break;
				case 4: //left
					x = Constants.WorldLeftBound() - this._spriteSize.x - this._unitOffset;
					this._vertical = false;
					this._positive = true;
					break;
			}

			if (x.Equals(0.0f))
			{
				x = Random.Range(Constants.WorldLeftBound() + (this._spriteSize.x) + this._unitOffset,
					Constants.WorldRightBound() - (this._spriteSize.x) - this._unitOffset);
			}
			else
			{
				y = Random.Range(Constants.WorldLowerBound() + (this._spriteSize.y) + this._unitOffset,
					Constants.WorldUpperBound() - (this._spriteSize.y) - this._unitOffset);
			}

			return new Vector3(x, y, 0.0f);
		}
		
	}
}

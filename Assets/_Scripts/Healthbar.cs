﻿using _Scripts;
using _Scripts.Player;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Healthbar : MonoBehaviour
{
    public List<Sprite> sprites;

    private Image _imageUI;
    [SerializeField]
    private GameObject _player; 
    private int index;

    public void Update()
    {
        index = sprites.Count() - _player.GetComponent<HealthPlayer>().Health;
        if ( _player.GetComponent<HealthPlayer>().Health > 0)
        {
            if ( index < 0 )
            {
                index = 0;
            }

            if ( index <= Constants.LivesMax - 1 )
            {
                _imageUI.sprite = sprites[index];
            }
        }
    }

    private void Awake()
    {
        _imageUI = gameObject.GetComponent<Image>();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SpriteController
{
    // If fitToScreenWidth is set to 1 then the width fits the screen width.
    // If it is set to anything over 1 then the sprite will not fit the screen width, it will be divided by that number.
    // If it is set to 0 then the sprite will not resize in that dimension.
    public static void ResizeSpriteToScreen(GameObject sprite, Camera camara, int fitToScreenWidth, int fitToScreenHeight)
    {
        SpriteRenderer sr = sprite.GetComponent<SpriteRenderer>();
        sr.drawMode = SpriteDrawMode.Sliced;

        sprite.transform.localScale = new Vector3(1, 1, 1);

        float width = sr.sprite.bounds.size.x;
        float height = sr.sprite.bounds.size.y;

        float worldScreenHeight = (float)(camara.orthographicSize * 2.0);
        float worldScreenWidth = (float)(worldScreenHeight * camara.aspect);

        if (fitToScreenWidth != 0)
        {
            Vector2 sizeX = new Vector2(worldScreenWidth / width / fitToScreenWidth, sprite.transform.localScale.y);
            sprite.transform.localScale = sizeX;
        }

        if (fitToScreenHeight != 0)
        {
            Vector2 sizeY = new Vector2(sprite.transform.localScale.x, worldScreenHeight / height / fitToScreenHeight);
            sprite.transform.localScale = sizeY;
        }
    }
}

﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Analytics;
using Random = UnityEngine.Random;
using TMPro;
using _Scripts.Player;
using _Scripts.Bomb;
using _Scripts.Enemy;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;
using _Scripts.Boosters;

namespace _Scripts
{
	public class GameController : MonoBehaviour
	{
        public TextMeshProUGUI ScoreText;
        public TextMeshProUGUI FinalScoreText;
        public TextMeshProUGUI HighScoreText;
        public TextMeshProUGUI InGameHighScoreText;
        public TextMeshProUGUI Pause_prompt;
        public Image Healthbar;
        public GameObject GameOverMenu;

        public static bool IsInputEnabled = true;
		[SerializeField] private GameObject _enemyPrefab;
		[SerializeField] private List<GameObject> _foodPrefabs = new List<GameObject>();
        [SerializeField] private GameObject _bombPrefab;
        [SerializeField] private GameObject _extraLifePrefab;

        private static GameController _instance = null;

		private readonly List<GameObject> _foodPool = new List<GameObject>();
		private readonly List<GameObject> _enemyPool = new List<GameObject>();

		private GameObject _player;
		private GameObject _bomb;
		private GameObject _extraLife;
        private bool _gameOver;
        private int _dificultyLimit;
        private int _nextExtraHealth;

        public bool GameOver
        {
            get { return _gameOver;  }
            set { _gameOver = value; }
        }

		private void Awake()
		{
			this._player = GameObject.FindGameObjectWithTag("Player");
            this._gameOver = false;
			this.LazyLoad();
			this.LoadFoodPool();
			this.LoadEnemy();
            this._dificultyLimit = Random.Range(1, Constants.LimitMax);
            this._bomb = Instantiate(_bombPrefab);
            this._extraLife = Instantiate(_extraLifePrefab);
            this._nextExtraHealth = Constants.ExtraLifeFrequency;
            Time.timeScale = 1;
        }

		void Start () {
            InGameHighScoreText.text = "Record: " + GetComponent<DataController>().HighScore.ToString();
            SoundController.Instance.PlayMusic(SoundController.Instance._clip_main_loop, 0.60f);
        }
	
		// Update is called once per frame
		void Update ()
		{
            if( !_gameOver)
            {
                if( Input.GetKeyDown(KeyCode.Space) )
                {
                    gameObject.GetComponent<Pause>().PauseGame();
                }
                AdjustDificulty();
                GetBomb();
                GetExtraLife();
            }
            if( _gameOver )
            {
                EndGame();
            }
		}

        private void OnEnable()
        {
            _gameOver = false;
        }

        private void LazyLoad()
		{
			if (_instance == null)
				_instance = this;
			else if(_instance != this )
				Destroy(this.gameObject);
		}

        public void DestroyAllEnemies()
        {
            List<GameObject> activeEnemies = _enemyPool.Where(e => e.activeSelf).ToList();

            foreach ( GameObject enemy in activeEnemies )
            {
                if ( enemy.GetComponent<MoveEnemy>().SpeedAverage >= Constants.EnemyMaxAverageSpeed )
                {
                    enemy.GetComponent<MoveEnemy>().SpeedAverage /= 2;
                }
                enemy.SetActive(false);
            }
        }

        private void LoadFoodPool()
		{
			int max = 0;

			for ( int index = 0 ; index < _foodPrefabs.Count ; index++)
			{
				switch (index)
				{
					case 0:
						max = Constants.CandyFoodPool;
						break;
					case 1:
						max = Constants.ChocolateFoodPool;
						break;
					case 2:
						max = Constants.PizzaFoodPool;
						break;
						
				}
				this.LoadFood(index, max);	
			}
		}

		private void LoadFood(int foodType, int max)
		{
			
			for (  int i = 0; i < max; i++)
			{
				GameObject item = Instantiate(_foodPrefabs[foodType]);
				
				item.transform.SetParent(this.gameObject.transform);
				this._foodPool.Add(item);
			}
		}

		private void AdjustDificulty()
		{
            int score = _player.GetComponent<Player.ScorePlayer>().Score;
			if ( score > this._dificultyLimit )
			{
				LoadEnemy();
				IncreaseEnemiesSpeed();
                this._dificultyLimit += Random.Range(Constants.LimitMin, Constants.LimitMax);
            }
        }

        private void LoadEnemy()
        {
            int max = Random.Range(0, Constants.DificultyFactor - 1);
            for (int i = 0; i <= max; i++)
            {
                if ( this.GetActiveEnemies() < Constants.EnemiesMaxQuantity )
                {
                    GameObject enemy = GetInactiveEnemy();
                    if (enemy != null)
                    {
                        enemy.SetActive(true);
                        enemy.GetComponent<MoveEnemy>().RestartUnit();
                        continue;
                    }
                    enemy = Instantiate(_enemyPrefab);
                    if (_enemyPool.Count > 0)
                    {
                        int index = Random.Range(0, _enemyPool.Count - 1);
                        enemy.GetComponent<Enemy.MoveEnemy>().SpeedAverage =
                            _enemyPool[index].GetComponent<Enemy.MoveEnemy>().SpeedAverage;
                    }
                    enemy.transform.SetParent(this.gameObject.transform);
                    _enemyPool.Add(enemy);
                }
                else
                {
                    return;
                }
            }
        }

        private void GetExtraLife()
        {
            if (!_extraLife.activeSelf && _player.GetComponent<ScorePlayer>().Score > this._nextExtraHealth )
            {
                this._nextExtraHealth += Constants.ExtraLifeFrequency; 
                _extraLife.GetComponent<MoveExtraLife>().RestartUnit();
                _extraLife.SetActive(true);
            }
        }

        private void GetBomb()
        {
            if ( !_bomb.activeSelf && this.GetActiveEnemies() == Constants.EnemiesMaxQuantity )
            {
                _bomb.GetComponent<MoveBomb>().RestartUnit();
                _bomb.SetActive(true);
            }
        }

        private int GetActiveEnemies()
        {
            return _enemyPool.Where(e => e.activeSelf).Count();
        }

        private GameObject GetInactiveEnemy()
        {
            return _enemyPool.FirstOrDefault(e => !e.activeSelf);
        }

        private void IncreaseFoodSpeed()
		{
			foreach (GameObject item in _foodPool)
			{
				item.GetComponent<Food.MoveFood>().SpeedAverage += 
                    Constants.SpeedAcceleration * Random.Range(1, Constants.DificultyFactor);
			}		
		}
		
		private void IncreaseEnemiesSpeed()
		{
            foreach (GameObject enemy in _enemyPool)
			{
                if (enemy.GetComponent<MoveEnemy>().SpeedAverage < Constants.EnemyMaxAverageSpeed)
                {
                    enemy.GetComponent<Animator>().speed += 0.01f;
                    enemy.GetComponent<Enemy.MoveEnemy>().SpeedAverage +=
                        Constants.SpeedAcceleration * Random.Range(1, Constants.DificultyFactor);
                }
			}		
		}

		private void DecreaseFoodPool()
		{
            int max = Random.Range(0, Constants.DificultyFactor - 1);
            for ( int i = 0; i <= max; i++ )
            {
                int index = Random.Range(0, _foodPool.Count - 1);
                _foodPool[index].SetActive(false);
                GameObject someFood = _foodPool[index];
                _foodPool.RemoveAt(index);
                Destroy(someFood);
            }
		}

        private void EndGame()
        {
            SoundController.Instance.MusicSource.Stop();
            SoundController.Instance.PlayEffect(SoundController.Instance._clip_game_over, 0.5f);
            EnableGameOverMenu();
            DisableGameUI();
            this.gameObject.SetActive(false);
        }

        public void QuitGame()
        {
            Application.Quit();
        }

        public void BackToMainMenu()
        {
            SceneManager.LoadScene(0);
        }

        private void DisableGameUI()
        {
            this.Healthbar.enabled = false;
            this.ScoreText.text = "";
            foreach(GameObject button in this.GetComponent<Pause>().InGameButtons)
            {
                button.SetActive(false);
            }
            InGameHighScoreText.text = "";
            _player.SetActive(false);
            _bomb.SetActive(false);
            GameObject.FindObjectOfType<Joystick>().gameObject.SetActive(false);
        }

        private void EnableGameOverMenu()
        {
            FinalScoreText.text = ScoreText.text + " points";
            GetComponent<DataController>().SubmitNewPlayerScore(_player.GetComponent<ScorePlayer>().Score);
            HighScoreText.text = "High Score: " + GetComponent<DataController>().HighScore + " points";
            GameOverMenu.SetActive(true);
            GameOverMenu.GetComponent<GameOverMenu>().enabled = true;
        }
 
	}
}

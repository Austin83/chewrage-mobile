﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    //Audio clips
    public AudioClip _clip_main_loop;
    public AudioClip _clip_game_over;
    public AudioClip _clip_boom;
    public AudioClip _clip_bounce;
    public AudioClip _clip_menu_loop;

    // Audio players components.
    public AudioSource EffectsSource;
    public AudioSource MusicSource;

    // Singleton instance.
    private static SoundController instance;

    public static SoundController Instance { get { return instance; } }

    // Initialize the singleton instance.
    private void Awake()
    {
        // If there is not already an instance of SoundManager, set it to this.
        if (instance == null)
        {
            instance = this;
        }
        //If an instance already exists, destroy whatever this object is to enforce the singleton.
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Play a single clip through the sound effects source.
    public void PlayEffect(AudioClip clip, float volume = 1.0f)
    {
        if( volume >= 0.0 && volume <= 1.0)
        {
            EffectsSource.volume = volume;
        }

        EffectsSource.clip = clip;
        EffectsSource.Play();
    }

    // Play a single clip through the music source.
    public void PlayMusic(AudioClip clip, float volume = 1.0f)
    {
        if (volume >= 0.0 && volume <= 1.0)
        {
            MusicSource.volume = volume;
        }

        MusicSource.clip = clip;
        MusicSource.Play();
    }
}

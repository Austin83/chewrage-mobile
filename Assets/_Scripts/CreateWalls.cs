﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateWalls : MonoBehaviour
{
    private Camera cam;
    private float _skin2D = 0.05f;

    GameObject top, bottom, left, right;
    GameObject[] barriers;

    void Start()
    {
        cam = Camera.main;
        Create2DBarriers();
        SetScales();
        SetPositions();
    }

    private void Create2DBarriers()
    {
        top = GameObject.CreatePrimitive(PrimitiveType.Cube);
        top.name = "Top";
        top.transform.localScale = new Vector3(0f, _skin2D, 0f);

        bottom = GameObject.CreatePrimitive(PrimitiveType.Cube);
        bottom.name = "Bottom";
        bottom.transform.localScale = new Vector3(0f, _skin2D, 0f);

        left = GameObject.CreatePrimitive(PrimitiveType.Cube);
        left.name = "Left";
        left.transform.localScale = new Vector3(_skin2D, 0f, 0f);

        right = GameObject.CreatePrimitive(PrimitiveType.Cube);
        right.name = "Right";
        right.transform.localScale = new Vector3(_skin2D, 0f, 0f);

        barriers = new GameObject[] { top, bottom, left, right };

        foreach (var b in barriers)
        {
            DestroyImmediate(b.GetComponent<Collider>());
            //no need to render these.
            DestroyImmediate(b.GetComponent<Renderer>());

            b.transform.parent = cam.transform;

            b.AddComponent<BoxCollider2D>();
            var rb = b.AddComponent<Rigidbody2D>();
            rb.isKinematic = true;
        }
    }

    private void SetScales()
    {
        var verticalSize = cam.orthographicSize * 2.0f;
        var horizontalSize = verticalSize * ((float)Screen.width / (float)Screen.height);

        top.transform.localScale = new Vector3(horizontalSize, _skin2D, 0f);
        bottom.transform.localScale = new Vector3(horizontalSize, _skin2D, 0f);
        left.transform.localScale = new Vector3(_skin2D, verticalSize, 0f);
        right.transform.localScale = new Vector3(_skin2D, verticalSize, 0f);
    }

    private void SetPositions()
    {
        //Camera rotations mess up the positions
        var camRot = cam.transform.rotation;
        cam.transform.rotation = Quaternion.identity;

        top.transform.position = cam.ViewportToWorldPoint(new Vector3(0.5f, 1f, 0.0f));
        bottom.transform.position = cam.ViewportToWorldPoint(new Vector3(0.5f, 0f, 0.0f));
        left.transform.position = cam.ViewportToWorldPoint(new Vector3(0f, 0.5f, 0.0f));
        right.transform.position = cam.ViewportToWorldPoint(new Vector3(1f, 0.5f, 0.0f));

        foreach (var b in barriers)
            b.transform.localPosition = ZeroZ(b.transform);

        //reset to the real rotation.
        cam.transform.rotation = camRot;
    }

    //Make each cube's position start at the camera's z position and extend outwards.
    private Vector3 ZeroZ(Transform t)
    {
        var pos = t.localPosition;
        pos.z = 0f;
        return pos;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DataController : MonoBehaviour
{
    private PlayerProgress playerProgress;
    private string gameDataFileName = "data.json";

    public int HighScore
    {
        get
        {
            return playerProgress.highScore;
        }

        set
        {
            playerProgress.highScore = value;
        }
    }

    void Awake()
    {
        LoadPlayerProgress();
    }

    public void SubmitNewPlayerScore(int newScore)
    {
        if ( newScore > HighScore )
        {
            HighScore = newScore;
            SavePlayerProgress();
        }
    }

    private void LoadPlayerProgress()
    {
        string filePath = Path.Combine(Application.persistentDataPath, gameDataFileName);

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            this.playerProgress = JsonUtility.FromJson<PlayerProgress>(dataAsJson);
        }
        else
        {
            PlayerProgress playerProgress = new PlayerProgress(0);
            File.WriteAllText(Path.Combine(Application.persistentDataPath, gameDataFileName), JsonUtility.ToJson(playerProgress));
            this.playerProgress = playerProgress;
        }
    }

    private void SavePlayerProgress()
    {
        string filePath = Path.Combine(Application.persistentDataPath, gameDataFileName);
        File.WriteAllText(filePath, JsonUtility.ToJson(playerProgress));
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerProgress
{
    public int highScore = 0;

    public int HighScore
    {
        get
        {
            return highScore;
        }
    }

    public PlayerProgress( int highScore)
    {
        this.highScore = highScore;
    }
}

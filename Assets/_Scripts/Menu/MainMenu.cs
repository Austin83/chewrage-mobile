﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject mainMenu;
    public GameObject instructionsMenu;

    private void Start()
    {
        SoundController.Instance.PlayMusic(SoundController.Instance._clip_menu_loop, 0.25f);
    }

    public void PlayGame()
    {
        SoundController.Instance.MusicSource.Stop();
        SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex + 1 );
    }

    public void InstructionsGame()
    {
        mainMenu.SetActive(false);
        instructionsMenu.SetActive(true);
    }

    public void BackInstructionsGame()
    {
        instructionsMenu.SetActive(false);
        mainMenu.SetActive(true);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
